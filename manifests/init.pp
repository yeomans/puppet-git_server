class git_server($user='git', $home="/var/lib/git", $ssh_authorized_keys={})
    {
    realize Package['git']

    user
        {
        "$user":
        home => "$home", 
        shell => "/usr/bin/git-shell", 
        system => true,
        require => Package['git'],
        } ->

    file
        {
        "$home":
        ensure => directory, 
        owner => "$user", 
        group => "$user", 
        mode => '0700', 
        } ->

    file
        {
        "$home/.ssh":
        ensure => directory, 
        owner => "$user", 
        group => "$user", 
        mode => '0700', 
        }
        
    create_resources(ssh_authorized_key, $ssh_authorized_keys, {"ensure"=>"present", "user"=>"$user"}) 
    }
